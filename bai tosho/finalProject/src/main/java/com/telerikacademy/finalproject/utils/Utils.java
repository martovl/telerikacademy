package com.telerikacademy.finalproject.utils;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

public class Utils {

	private static Properties uiMappings =  PropertiesManager.PropertiesManagerEnum.INSTANCE.getUiMappings();
	private static Properties configProperties =  PropertiesManager.PropertiesManagerEnum.INSTANCE.getConfigProperties();
	public static final Logger LOG = LogManager.getRootLogger();

	public static WebDriver getWebDriver() {
		LOG.info("Initializing WebDriver");
		return CustomWebDriverManager.CustomWebDriverManagerEnum.INSTANCE.getDriver();
	}

	public static void tearDownWebDriver() {
		LOG.info("Quitting WebDriver");
		CustomWebDriverManager.CustomWebDriverManagerEnum.INSTANCE.quitDriver();
	}

	public static String getUIMappingByKey(String key, Object... arguments) {
		String value = uiMappings.getProperty(key);

		if (arguments.length > 0){
			value = String.format(Utils.getUIMappingByKey(key), arguments);
		}

		return value != null ? value : key;
	}

	public static Properties getConfigProperties() {
		return PropertiesManager.PropertiesManagerEnum.INSTANCE.getConfigProperties();
	}

	public static String getConfigPropertyByKey(String key){
		return configProperties.getProperty(key);
	}

	public static String generateCommonLangPassword() {
		String upperCaseLetters = RandomStringUtils.random(2, 65, 90, true, true);
		String lowerCaseLetters = RandomStringUtils.random(2, 97, 122, true, true);
		String numbers = RandomStringUtils.randomNumeric(2);
		String specialChar = RandomStringUtils.random(2, 35, 37, false, false);
		String totalChars = RandomStringUtils.randomAlphanumeric(2);
		String combinedChars = upperCaseLetters.concat(lowerCaseLetters)
				.concat(numbers)
				.concat(specialChar)
				.concat(totalChars);
		List<Character> pwdChars = combinedChars.chars()
				.mapToObj(c -> (char) c)
				.collect(Collectors.toList());
		Collections.shuffle(pwdChars);
		String password = pwdChars.stream()
				.collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
				.toString();
		return password;
	}

}
