package com.telerikacademy.finalproject.utils;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

import static com.telerikacademy.finalproject.utils.Constants.*;

public class UserActions {


    final WebDriver driver;

    public UserActions() {
        this.driver = Utils.getWebDriver();
    }

    public static void loadBrowser(String baseUrlKey) {
        Utils.getWebDriver().get(Utils.getConfigPropertyByKey(baseUrlKey));
    }

    public static void quitDriver() {
        Utils.tearDownWebDriver();
    }

    public WebDriver getDriver() {
        return driver;
    }

    //############# ELEMENT OPERATIONS #########

    public void clickElement(String key, Object... arguments) {
        String locator = Utils.getUIMappingByKey(key, arguments);

        Utils.LOG.info("Clicking on element " + key);
        WebElement element = driver.findElement(By.xpath(locator));
        element.click();
    }

    public void typeValueInField(String value, String field, Object... fieldArguments) {
        String locator = Utils.getUIMappingByKey(field, fieldArguments);
        WebElement element = driver.findElement(By.xpath(locator));
        element.sendKeys(value);
    }

    public void switchToIFrame(String iframe) {
        WebElement iFrame = driver.findElement(By.xpath(Utils.getUIMappingByKey(iframe)));
        getDriver().switchTo().frame(iFrame);
    }

    public void scrollToElement(String key) {
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(key)));
        Utils.LOG.info("Scrolling to element " + key);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
    }

    public void mouseHoverElement(String key) {
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(key)));
        Utils.LOG.info("Hovering on element " + key);
        Actions a = new Actions(driver);
        a.moveToElement(element).perform();
    }

    //Creates random String
    public String createRandomString(int n) {
        Utils.LOG.info("Creating random string");
        return RandomStringUtils.randomAlphabetic(n);
    }

    //TC028_memberSuccessfulSendsRequest
    public void sentUserRequest() {
        Utils.LOG.info("User send connection request");
        isElementPresentUntilTimeout("USERS.SectionUsers", 30);
        mouseHoverElement("USERS.SectionUsers");
        clickElement("USERS.SectionUsersDropDownMenu", 30);
        clickElement("USERS.SectionUsers");
        isElementPresentUntilTimeout("USERS.ListUsers", 30);
        isElementPresentUntilTimeout("USERS.SearchedUser", 30);
        clickElement("USERS.SearchedUser");
        isElementPresentUntilTimeout("USERS.ConnectButton", 30);
        waitElementToBeClickable("USERS.ConnectButton", 20);
        clickElement("USERS.ConnectButton");
        isElementPresentUntilTimeout("USERS.Sent(Reject)RequestButton", 30);
    }

    //TC029_memberSuccessfulAcceptRequest
    public void acceptUserRequest() {
        Utils.LOG.info("User accepts friend request");
        isElementPresentUntilTimeout("navigation.UsersMenu", 30);
        mouseHoverElement("navigation.UsersMenu");
        isElementPresentUntilTimeout("USERS.RequestYellowBellDropDown", 30);
        waitElementToBeClickable("USERS.RequestYellowBellDropDown", 20);
        clickElement("USERS.RequestYellowBellDropDown");
        waitElementToBeClickable("USERS.RequestSender", 30);
        clickElement("USERS.RequestSender");
        isElementPresentUntilTimeout("USERS.ConfirmButton", 30);
        waitElementToBeClickable("USERS.ConfirmButton", 20);
        clickElement("USERS.ConfirmButton");
        isElementPresentUntilTimeout("USERS.DisconnectButton", 30);
    }

    //TC030_memberSuccessfulDisconnectsFriend
    public void userDisconnectsFriend() {
        Utils.LOG.info("User disconnects friend");
        assertElementPresent("navigation.UsersMenu");
        mouseHoverElement("navigation.UsersMenu");
        waitElementToBeClickable("USERS.FriendsDropDownItem", 30);
        clickElement("USERS.FriendsDropDownItem");
        isElementPresentUntilTimeout("USERS.ListFriends", 30);
        waitElementToBeClickable("USERS.SearchedFriendDisconnecting", 30);
        clickElement("USERS.SearchedFriendDisconnecting");
        waitElementToBeClickable("USERS.DisconnectButton", 30);
        clickElement("USERS.DisconnectButton");
        isElementPresentUntilTimeout("USERS.ConnectButton", 30);
    }

    // TC040_memberSuccessfulCreatesConnectionsPostWithDescription
    public void selectByValue(String key, String value) {
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(key)));
        Select selectObject = new Select(element);
        selectObject.selectByValue(value);
    }

    //TC040_memberSuccessfulCreatesConnectionsPostWithDescription
    public void userCreatesConnectionsPost() throws URISyntaxException {
        Utils.LOG.info("User creates connection post");
        isElementPresentUntilTimeout("navigation.LatestPosts", 40);
        mouseHoverElement("navigation.LatestPosts");
        assertElementPresent("POSTS.NewPostDropDownItem");
        clickElement("POSTS.NewPostDropDownItem");
        isElementPresentUntilTimeout("POSTS.PostPicture", 40);
        selectByValue("POSTS.PostVisibility", "2");
        isElementPresentUntilTimeout("POSTS.Title", 40);
        typeValueInField(CONNECTIONS_TITLE_POST, "POSTS.Title");
        isElementPresentUntilTimeout("POSTS.Description", 40);
        typeValueInField(CONNECTIONS_POST_DESCRIPTION, "POSTS.Description");
        isElementPresentUntilTimeout("POSTS.ChooseFilePostButton", 40);
        URL url = getClass().getResource("/UploadFiles/milk.jpg");
        File file = Paths.get(url.toURI()).toFile();
        typeValueInField(file.getAbsolutePath(), "POSTS.ChooseFilePostButton");
        clickElement("POSTS.SaveButton");
    }

    //TC043_memberSuccessfulUpdatesFirstNameANDLastNameAndAdditionalInformationForItself
    public void updateUsersInfo() {
        Utils.LOG.info("User updates information about itself");
        isElementPresentUntilTimeout("navigation.Profile", 30);
        mouseHoverElement("navigation.Profile");
        isElementPresentUntilTimeout("PROFILE.EditDropDownItem", 30);
        clickElement("PROFILE.EditDropDownItem");

        driver.findElement(By.xpath("//input[@id='firstName']")).clear();
        isElementPresentUntilTimeout("PROFILE.FirstNameField", 25);
        typeValueInField(Constants.UPDATED_FIRST_NAME, "PROFILE.FirstNameField");


        driver.findElement(By.xpath("//input[@id='lastName']")).clear();
        isElementPresentUntilTimeout("PROFILE.LastNameField", 25);
        typeValueInField(Constants.UPDATED_LAST_NAME, "PROFILE.LastNameField");

        isElementPresentUntilTimeout("PROFILE.NationalityField", 25);
        typeValueInField(Constants.UPDATED_NATIONALITY, "PROFILE.NationalityField");

        isElementPresentUntilTimeout("PROFILE.GenderField", 25);
        typeValueInField(Constants.UPDATED_GENDER, "PROFILE.GenderField");

        driver.findElement(By.xpath("//input[@id='age']")).clear();
        isElementPresentUntilTimeout("PROFILE.AgeField", 25);
        typeValueInField(Constants.UPDATED_AGE, "PROFILE.AgeField");

        driver.findElement(By.xpath("//textarea[@id='description']")).clear();
        isElementPresentUntilTimeout("PROFILE.AdditionalInfoField", 25);
        typeValueInField(Constants.UPDATED_ADDITIONAL_INFO, "PROFILE.AdditionalInfoField");

        clickElement("PROFILE.SaveChangesButton");
    }

    //TC049_memberSuccessfulChangesProfilePictureJPEGFormat
    public void updateUserProfilePicture() throws URISyntaxException {
        Utils.LOG.info("User updates his profile picture");
        isElementPresentUntilTimeout("navigation.Profile", 30);
        mouseHoverElement("navigation.Profile");
        isElementPresentUntilTimeout("PROFILE.EditDropDownItem", 30);
        waitElementToBeClickable("PROFILE.EditDropDownItem", 15);
        clickElement("PROFILE.EditDropDownItem");
        isElementPresentUntilTimeout("PROFILE.ChooseFileUserButton", 30);
        URL url = getClass().getResource("/UploadFiles/2.jpg");
        File file = Paths.get(url.toURI()).toFile();
        isElementPresentUntilTimeout("PROFILE.ChooseFileUserButton", 15);
        typeValueInField(file.getAbsolutePath(), "PROFILE.ChooseFileUserButton");
        clickElement("PROFILE.SaveChangesButton");
    }

    //TC_063 AdminSuccessfulCreatesNewCategory
    public void createAdminCategory() throws URISyntaxException {
        Utils.LOG.info("Admin create category");
        isElementPresentUntilTimeout("navigation.Settings", 40);
        mouseHoverElement("navigation.Settings");
        waitElementToBeClickable("ADMIN.Categories", 25);
        clickElement("ADMIN.Categories");
        waitElementToBeClickable("ADMIN.AddButton", 40);
        clickElement("ADMIN.AddButton");
        isElementPresentUntilTimeout("ADMIN.ChooseFileCategoryButton", 40);
        URL url = getClass().getResource("/UploadFiles/drinks.png");
        File file = Paths.get(url.toURI()).toFile();
        typeValueInField(file.getAbsolutePath(), "ADMIN.ChooseFileCategoryButton");
        isElementPresentUntilTimeout("ADMIN.CategoryName", 40);
        waitElementToBeClickable("ADMIN.CategoryName", 30);
        typeValueInField(CATEGORY_NAME, "ADMIN.CategoryName");
        clickElement("ADMIN.SaveButton");
        isElementPresentUntilTimeout("ADMIN.AddButton", 40);
    }

    //TC_064 AdminSuccessfulDeletesCategory
    public void deleteAdminCategory() {
        Utils.LOG.info("Admin delete category");
        isElementPresentUntilTimeout("ADMIN.EditDesiredCategory", 20);
        waitElementToBeClickable("ADMIN.EditDesiredCategory", 20);
        clickElement("ADMIN.EditButton");
        waitElementToBeClickable("ADMIN.DeleteButton", 30);
        clickElement("ADMIN.DeleteButton");
        waitElementToBeClickable("ADMIN.YesButton", 30);
        clickElement("ADMIN.YesButton");
        isElementPresentUntilTimeout("ADMIN.AddButton", 30);
    }

    //TC_065 AdminSuccessfulDeletesPost
    public void deleteAdminConnectionsPost() {
        Utils.LOG.info("Admin delete connection post");
        isElementPresentUntilTimeout("navigation.Settings", 40);
        mouseHoverElement("navigation.Settings");
        clickElement("ADMIN.Posts");
        isElementPresentUntilTimeout("ADMIN.ListPosts", 40);
        typeValueInField(CONNECTIONS_TITLE_POST, "ADMIN.SearchKeywordInput");
        clickElement("ADMIN.SearchButton");
        isElementPresentUntilTimeout("ADMIN.SearchedPostForDeleting", 40);
        clickElement("ADMIN.SearchedPostForDeleting");
        isElementPresentUntilTimeout("ADMIN.EditItem", 40);
        waitElementToBeClickable("ADMIN.EditItem", 40);
        clickElement("ADMIN.EditItem");
        isElementPresentUntilTimeout("ADMIN.DeleteButton", 25);
        waitElementToBeClickable("ADMIN.DeleteButton", 40);
        clickElement("ADMIN.DeleteButton");
        isElementPresentUntilTimeout("ADMIN.YesButton", 40);
        clickElement("ADMIN.YesButton");
        isElementPresentUntilTimeout("ADMIN.FirstName", 40);
    }

    //TC073_successfulRegistrationWithPasswordLength8SymbolsContainingAllMandatoryCharacters
    public void registerUser() throws URISyntaxException {
        Utils.LOG.info("Make successful registration");
        clickElement("loginPage.RegisterButton");
        isElementPresentUntilTimeout("REGISTRATION.Header", 20);

        isElementPresentUntilTimeout("REGISTRATION.EmailInput", 25);
        typeValueInField(createRandomString(5) + REGISTRATION_USERNAME, "REGISTRATION.EmailInput");

        isElementPresentUntilTimeout("REGISTRATION.Age", 25);
        typeValueInField(REGISTRATION_AGE, "REGISTRATION.Age");

        isElementPresentUntilTimeout("REGISTRATION.PasswordInput", 25);
        typeValueInField(PASSWORD, "REGISTRATION.PasswordInput");

        isElementPresentUntilTimeout("REGISTRATION.PasswordConfirmationInput", 25);
        typeValueInField(PASSWORD, "REGISTRATION.PasswordConfirmationInput");

        isElementPresentUntilTimeout("REGISTRATION.FirstNameInput", 25);
        typeValueInField(REGISTRATION_FIRST_NAME, "REGISTRATION.FirstNameInput");

        isElementPresentUntilTimeout("REGISTRATION.LastNameInput", 25);
        typeValueInField(REGISTRATION_LAST_NAME, "REGISTRATION.LastNameInput");


        isElementPresentUntilTimeout("REGISTRATION.Nationality", 25);
        typeValueInField(REGISTRATION_NATIONALITY, "REGISTRATION.Nationality");

        isElementPresentUntilTimeout("REGISTRATION.Gender", 25);
        typeValueInField(REGISTRATION_GENDER, "REGISTRATION.Gender");

        isElementPresentUntilTimeout("REGISTRATION.AboutInput", 25);
        typeValueInField(REGISTRATION_ADDITIONAL_INFO, "REGISTRATION.AboutInput");

        isElementPresentUntilTimeout("REGISTRATION.Visibility", 25);
        typeValueInField(REGISTRATION_PROFILE_PICTURE_VISIBILITY, "REGISTRATION.Visibility");

        URL url = getClass().getResource("/UploadFiles/drinks.png");
        File file = Paths.get(url.toURI()).toFile();
        typeValueInField(file.getAbsolutePath(), "REGISTRATION.ChooseFileProfilePicture");

        isElementPresentUntilTimeout("REGISTRATION.RegisterButton", 25);
        waitElementToBeClickable("REGISTRATION.RegisterButton", 25);
        clickElement("REGISTRATION.RegisterButton");
    }


    //TC_054_memberSuccessfulLikesFriendPost
    public void userLikesFriendPost() {
        Utils.LOG.info("User likes friend post");
        assertElementPresent("navigation.LatestPosts");
        mouseHoverElement("navigation.LatestPosts");
        waitElementToBeClickable("Navigation.MyFriendsPost", 30);
        clickElement("Navigation.MyFriendsPost");
        isElementPresentUntilTimeout("Like.RecentPost", 30);
        isElementPresentUntilTimeout("Like.SearchedPost", 30);
        waitElementToBeClickable("Like.SearchedPost", 30);
        clickElement("Like.SearchedPost");
        isElementPresentUntilTimeout("Like.SearchedPostHeader", 30);
        waitElementToBeClickable("Friends.LikeButton", 30);
        clickElement("Friends.LikeButton");
    }

    //TC_056_memberSuccessfulDislikesFriendPost
    public void userDislikesFriendPost() {
        Utils.LOG.info("User dislikes friend post");
        assertElementPresent("navigation.LatestPosts");
        mouseHoverElement("navigation.LatestPosts");
        waitElementToBeClickable("Navigation.MyFriendsPost", 30);
        clickElement("Navigation.MyFriendsPost");
        isElementPresentUntilTimeout("Dislike.RecentPost", 30);
        isElementPresentUntilTimeout("Dislike.SearchedPost", 30);
        waitElementToBeClickable("Dislike.SearchedPost", 20);
        clickElement("Dislike.SearchedPost");
        isElementPresentUntilTimeout("Dislike.SearchedPostHeader", 30);
        waitElementToBeClickable("Friends.DislikeButton", 20);
        clickElement("Friends.DislikeButton");
    }

    //TC058_memberSuccessfulCommentsFriendPost
    public void userCommentFriendPost() {
        Utils.LOG.info("User comments friend post");
        assertElementPresent("navigation.LatestPosts");
        mouseHoverElement("navigation.LatestPosts");
        waitElementToBeClickable("Navigation.MyFriendsPost", 30);
        clickElement("Navigation.MyFriendsPost");
        isElementPresentUntilTimeout("Dislike.RecentPost", 30);
        isElementPresentUntilTimeout("Dislike.SearchedPost", 30);
        waitElementToBeClickable("Dislike.SearchedPost", 20);
        clickElement("Dislike.SearchedPost");
        isElementPresentUntilTimeout("Dislike.SearchedPostHeader", 30);
        isElementPresentUntilTimeout("Comment.Field", 30);
        waitElementToBeClickable("Comment.Field", 20);
        typeValueInField(COMMENT_FRIEND_POST, "Comment.Field");
        waitElementToBeClickable("Comment.SendButton", 20);
        clickElement("Comment.SendButton");
    }
    //############# WAITS #########

    public boolean isElementPresentUntilTimeout(String locator, int timeout, Object... arguments) {
        try {
            String xpath = Utils.getUIMappingByKey(locator, arguments);
            Integer defaultTimeout = Integer.parseInt(Utils.getConfigPropertyByKey("config.defaultTimeoutSeconds"));
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(xpath)));
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    public boolean waitElementToBeClickable(String locator, int timeout, Object... arguments) {
        try {
            String xpath = Utils.getUIMappingByKey(locator, arguments);
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
            return true;
        } catch (Exception exception) {
            return false;
        }
    }


    //############# ASSERTS #########

    public void assertElementPresent(String locator) {
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
    }

    public void assertElementNotPresent(String locator) {
        Assert.assertEquals(0, driver.findElements(By.xpath(Utils.getUIMappingByKey(locator))).size());
    }

    //TC028_memberSuccessfulSendsRequest
    public boolean isRequestSent() {
        return isElementPresentUntilTimeout("USERS.Sent(Reject)RequestButton", 50);
    }

    //TC029_memberSuccessfulAcceptRequest
    public boolean isUserFriend() {
        Utils.LOG.info("Check if users are connected");
        assertElementPresent("navigation.UsersMenu");
        mouseHoverElement("navigation.UsersMenu");
        assertElementPresent("USERS.FriendsDropDownItem");
        waitElementToBeClickable("USERS.FriendsDropDownItem", 20);
        clickElement("USERS.FriendsDropDownItem");
        isElementPresentUntilTimeout("USERS.ListFriends", 30);
        return isElementPresentUntilTimeout("USERS.SearchedFriend", 30);
    }

    //TC030_memberSuccessfulDisconnectsFriend
    public boolean isUsersDisconnected() {
        Utils.LOG.info("Check if users are disconnected");
        isElementPresentUntilTimeout("USERS.SectionUsers", 30);
        clickElement("USERS.SectionUsers");
        isElementPresentUntilTimeout("USERS.ListUsers", 30);
        isElementPresentUntilTimeout("USERS.SearchedFriendDisconnecting", 30);
        waitElementToBeClickable("USERS.SearchedFriendDisconnecting", 15);
        clickElement("USERS.SearchedFriendDisconnecting");
        return isElementPresentUntilTimeout("USERS.ConnectButton", 30);
    }

    //TC040_memberSuccessfulCreatesConnectionsPostWithDescription()
    public boolean isPostCreated() {
        Utils.LOG.info("Check if post is created");
        isElementPresentUntilTimeout("POSTS.ListAllPosts", 30);
        scrollToElement("POSTS.CreatedPost");
        //waitElementToBeClickable("POSTS.CreatedPost", 50);
        clickElement("POSTS.CreatedPost");
        return isElementPresentUntilTimeout("POSTS.DescriptionUnderPost", 50);
    }


    //TC049_memberSuccessfulChangesProfilePictureJPEGFormat()
    public boolean isProfilePictureChanged() {
        Utils.LOG.info("Check if profile picture is changed");
        return isElementPresentUntilTimeout("PROFILE.EditButton", 50);
    }

    //TC_054_memberSuccessfulLikesFriendPost
    public void isFriendPostLiked() {
        Utils.LOG.info("Check if friend post is liked");
        isElementPresentUntilTimeout("Friends.DislikeButton", 30);
        assertElementPresent("Friends.DislikeButton");
    }

    //TC_056_memberSuccessfulDislikesFriendPost
    public void isFriendPostDisliked() {
        Utils.LOG.info("Check if friend post is disliked");
        isElementPresentUntilTimeout("Friends.LikeButton", 30);
        assertElementPresent("Friends.LikeButton");

    }

    //TC058_memberSuccessfulCommentsFriendPost
    public void isFriendPostCommented() {
        Utils.LOG.info("Check if friend post is commented");
        isElementPresentUntilTimeout("Comment.SuccessfulComment", 30);
        assertElementPresent("Comment.SuccessfulComment");
    }

    //TC_063 AdminSuccessfulCreatesNewCategory()
    public void isCategoryCreated() {
        Utils.LOG.info("Check if category is created");
        isElementPresentUntilTimeout("ADMIN.CreatedCategory", 30);
        assertElementPresent("ADMIN.CreatedCategory");
    }

    //TC064_adminSuccessfulDeletesCategory()
    public void isCategoryDeleted() {
        Utils.LOG.info("Check if category is deleted");
        assertElementNotPresent("ADMIN.EditDesiredCategory");
    }

    //TC065_adminSuccessfulDeletesPost()
    public void isConnectionsPostDeleted() {
        Utils.LOG.info("Check if connection post is deleted");
        isElementPresentUntilTimeout("navigation.Settings", 40);
        mouseHoverElement("navigation.Settings");
        clickElement("ADMIN.Posts");
        isElementPresentUntilTimeout("ADMIN.ListPosts", 40);
        typeValueInField(CONNECTIONS_TITLE_POST, "ADMIN.SearchKeywordInput");
        waitElementToBeClickable("ADMIN.SearchButton", 30);
        clickElement("ADMIN.SearchButton");
        isElementPresentUntilTimeout("ADMIN.SearchedPostForDeleting", 40);
        Utils.LOG.info("Xpath does NOT exist: " + "ADMIN.SearchedPostForDeleting");
        Assert.assertEquals(0, driver.findElements(By.xpath(Utils.getUIMappingByKey("ADMIN.SearchedPostForDeleting"))).size());
    }
}

