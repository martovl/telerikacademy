package com.telerikacademy.finalproject.utils;

public class Constants {


    //USERS DATA
    public final static String EMAIL_USER1_ADMIN = "finalprojectnarnia@gmail.com";
    public final static String EMAIL_USER2_REGULAR = "nevina_82@abv.bg";
    public final static String EMAIL_USER3_REGULAR = "irka_1982@abv.bg";
    public final static String EMAIL_USER4_REGULAR = "nevenamalinova@outlook.com";
    public final static String EMAIL_USER5_REGULAR = "martin.vlachkov22@gmail.com";
    public final static String PASSWORD = "Narniaqa22@";

    //USER WRONG CREDENTIALS
    public static final String WRONG_USER_EMAIL = "hello@gmail.com";
    public static final String WRONG_USER_PASSWORD = "test123";

    //User5 Updated Data
    public static final String UPDATED_FIRST_NAME = "Martin";
    public static final String UPDATED_LAST_NAME = "Vlachkov";
    public static final String UPDATED_NATIONALITY = "American";
    public static final String UPDATED_GENDER = "Female";
    public static final String UPDATED_AGE = "35";
    public static final String UPDATED_ADDITIONAL_INFO = "I like testing!";

    //Alerts messages
    public static final String ALERT_WRONG_USERNAME_PASSWORD = "Wrong username or password.";

    //Posts
    public static final String CONNECTIONS_TITLE_POST = "Milk";
    public static final String CONNECTIONS_POST_DESCRIPTION = "Milk helps build strong bones because it's full of calcium and vitamin";
    public static final String CATEGORY_NAME = "Children drinks";


    //Register
    public static final String REGISTRATION_USERNAME = "@gmail.com";
    public static final String REGISTRATION_FIRST_NAME = "Martin";
    public static final String REGISTRATION_LAST_NAME = "Vlachkov";
    public static final String REGISTRATION_NATIONALITY = "American";
    public static final String REGISTRATION_GENDER = "Female";
    public static final String REGISTRATION_AGE = "35";
    public static final String REGISTRATION_ADDITIONAL_INFO = "I like testing!";
    public static final String REGISTRATION_PROFILE_PICTURE_VISIBILITY = "public";

    //Comment
    public static final String COMMENT_FRIEND_POST= "This is awesome post!";
}

