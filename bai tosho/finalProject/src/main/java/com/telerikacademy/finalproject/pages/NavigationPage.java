package com.telerikacademy.finalproject.pages;

import org.junit.Test;

public class NavigationPage extends BasePage {
    public NavigationPage() {
        super("base.url");
    }
    public final String homeButton = "navigation.Home";
    public final String signInButton = "navigation.SignIn";
    public final String usersMenu = "navigation.UsersMenu";
    public final String logOut ="navigation.LogOut";



}