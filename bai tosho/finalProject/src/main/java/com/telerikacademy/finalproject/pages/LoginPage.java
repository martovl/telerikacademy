package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Constants;
import com.telerikacademy.finalproject.utils.Utils;


public class LoginPage extends BasePage {

    public LoginPage() {
        super("login.url");
    }

    public void logIn(String userName, String password) {
        assertPageNavigated();
        actions.isElementPresentUntilTimeout("loginPage.LoginButton", 20);
        actions.assertElementPresent("loginPage.LoginButton");
        actions.assertElementPresent("loginPage.EmailField");
        actions.isElementPresentUntilTimeout("loginPage.EmailField",20);
        actions.typeValueInField(userName, "loginPage.EmailField");
        actions.assertElementPresent("loginPage.PasswordField");
        actions.isElementPresentUntilTimeout("loginPage.PasswordField",20);
        actions.typeValueInField(password, "loginPage.PasswordField");
        actions.waitElementToBeClickable("loginPage.LoginButton",20);
        actions.clickElement("loginPage.LoginButton");
    }

    public boolean isUserLoggedIn() {
        return actions.isElementPresentUntilTimeout("navigation.UsersMenu", 20);
    }

    public void logOut() {
        actions.waitElementToBeClickable("navigation.LogOut", 30);
        actions.clickElement("navigation.LogOut");
        actions.isElementPresentUntilTimeout("loginPage.LoginButton", 20);
    }

    public boolean isUserLoggedOut() {
        return actions.isElementPresentUntilTimeout("loginPage.LoginButton", 20);
    }

    public void logInWithWrongPassword(){
        Utils.LOG.info("Performing Login method");
        String password = Constants.WRONG_USER_PASSWORD;

        logIn(Constants.EMAIL_USER1_ADMIN,password);
    }

    public void logInWithWrongEmail(){
        Utils.LOG.info("Performing Login method");
        String email = Constants.WRONG_USER_EMAIL;

        logIn(email,Constants.PASSWORD);
    }

    public void logInWithEmptyFieldForPassword(){
        Utils.LOG.info("Performing Login method");
        String email = Constants.EMAIL_USER1_ADMIN;
        String password = "";

        logIn(email,password);
    }

    public void logInWithEmptyFieldForEmail(){
        Utils.LOG.info("Performing Login method");
        String email = " ";
        String password = Constants.PASSWORD;

        logIn(email,password);
    }
}
