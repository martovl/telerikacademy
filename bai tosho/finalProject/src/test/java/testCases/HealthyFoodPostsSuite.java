package testCases;

import com.telerikacademy.finalproject.pages.LoginPage;
import org.junit.*;
import org.junit.runners.MethodSorters;

import java.net.URISyntaxException;

import static com.telerikacademy.finalproject.utils.Constants.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)


public class HealthyFoodPostsSuite extends BaseTest {
    LoginPage loginPage = new LoginPage();

    @Before
    public void login() {
        loginPage.navigateToPage();

    }

    @After
    public void logout() {
        loginPage.logOut();
        Assert.assertTrue(loginPage.isUserLoggedOut());

    }

    //TC_040 MemberSuccessfulCreatesConnectionsPostWithDescription
    //TC_065 AdminSuccessfulDeletesConnectionPost
    @Test
    public void TC040_memberSuccessfulCreatesConnectionsPostWithDescription_AND_TC065_adminSuccessfulDeletesConnectionPost()
            throws URISyntaxException {

        loginPage.logIn(EMAIL_USER2_REGULAR, PASSWORD);
        Assert.assertTrue(loginPage.isUserLoggedIn());

        actions.userCreatesConnectionsPost();
        Assert.assertTrue(actions.isPostCreated());
        loginPage.logOut();
        Assert.assertTrue(loginPage.isUserLoggedOut());

        loginPage.logIn(EMAIL_USER1_ADMIN, PASSWORD);
        loginPage.isUserLoggedIn();

        actions.deleteAdminConnectionsPost();
        actions.isConnectionsPostDeleted();
    }

    @Test
    public void TC054_memberSuccessfulLikesFriendPost() {

        loginPage.logIn(EMAIL_USER3_REGULAR, PASSWORD);
        Assert.assertTrue(loginPage.isUserLoggedIn());

        actions.userLikesFriendPost();

        actions.isFriendPostLiked();

    }

    @Test
    public void TC056_memberSuccessfulDislikesFriendPost() {

        loginPage.logIn(EMAIL_USER3_REGULAR, PASSWORD);
        Assert.assertTrue(loginPage.isUserLoggedIn());

        actions.userDislikesFriendPost();
        actions.isFriendPostDisliked();

    }

    @Test
    public void TC058_memberSuccessfulCommentsFriendPost() {

        loginPage.logIn(EMAIL_USER3_REGULAR, PASSWORD);
        Assert.assertTrue(loginPage.isUserLoggedIn());

        actions.userCommentFriendPost();
        actions.isFriendPostCommented();
    }


    // TC_063 AdminSuccessfulCreatesNewCategory
    // TC_064 AdminSuccessfulDeletesCategory
    @Test
    public void TC063_adminSuccessfulCreatesNewCategory_AND_TC_064_adminSuccessfulDeletesCategory()
            throws URISyntaxException {

        loginPage.logIn(EMAIL_USER1_ADMIN, PASSWORD);
        Assert.assertTrue(loginPage.isUserLoggedIn());

        actions.createAdminCategory();
        actions.isCategoryCreated();
        actions.deleteAdminCategory();
        actions.isCategoryDeleted();
    }
}
