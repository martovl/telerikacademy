package testCases;

import com.telerikacademy.finalproject.pages.LoginPage;
import org.junit.*;
import org.junit.runners.MethodSorters;

import java.net.URISyntaxException;

import static com.telerikacademy.finalproject.utils.Constants.*;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class HealthyFoodUsersSuite extends BaseTest {
    LoginPage loginPage = new LoginPage();


    @Before
    public void login() {
        loginPage.navigateToPage();

    }

    @After
    public void logout() {
        loginPage.logOut();
        Assert.assertTrue(loginPage.isUserLoggedOut());

    }

    // TC028_memberSuccessfulSendsRequest()
    // TC029_memberSuccessfulAcceptRequest
    @Test
    public void TC028_memberSuccessfulSendsRequest_AND_TC029_memberSuccessfulAcceptRequest() {
        loginPage.logIn(EMAIL_USER1_ADMIN, PASSWORD);
        loginPage.isUserLoggedIn();
        actions.sentUserRequest();
        Assert.assertTrue(actions.isRequestSent());

        loginPage.logOut();

        loginPage.logIn(EMAIL_USER2_REGULAR, PASSWORD);
        Assert.assertTrue(loginPage.isUserLoggedIn());
        actions.acceptUserRequest();
        Assert.assertTrue(actions.isUserFriend());
    }

    @Test
    public void TC030_memberSuccessfulDisconnectsFriend() {
        loginPage.logIn(EMAIL_USER5_REGULAR, PASSWORD);
        loginPage.isUserLoggedIn();
        actions.userDisconnectsFriend();
        Assert.assertTrue(actions.isUsersDisconnected());
    }

    @Test
    public void TC043_memberSuccessfulUpdatesFirstNameANDLastNameAndAdditionalInformationForItself() {
        loginPage.logIn(EMAIL_USER5_REGULAR, PASSWORD);
        loginPage.isUserLoggedIn();

        actions.updateUsersInfo();

        actions.assertElementPresent("UPDATED.Profile.Name");
        actions.assertElementPresent("UPDATED.Profile.LastName");
        actions.assertElementPresent("UPDATED.Profile.Age");
        actions.assertElementPresent("UPDATED.Profile.Gender");
        actions.assertElementPresent("UPDATED.Profile.Nationality");
        actions.assertElementPresent("UPDATED.Profile.AdditionalInfo");
    }

    @Test
    public void TC049_memberSuccessfulChangesProfilePictureJPEGFormat() throws URISyntaxException {
        loginPage.logIn(EMAIL_USER1_ADMIN, PASSWORD);
        loginPage.isUserLoggedIn();

        actions.updateUserProfilePicture();
        Assert.assertTrue(actions.isProfilePictureChanged());
    }


}
