package testCases;

import com.telerikacademy.finalproject.pages.LoginPage;
import com.telerikacademy.finalproject.utils.Constants;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.net.URISyntaxException;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class HealthyFoodLoginSuite extends BaseTest {
    LoginPage loginPage = new LoginPage();

    @Before
    public void login() {
        loginPage.navigateToPage();
    }

    @Test
    public void TC066_successfulLogInAndLogOut() {
        //log in
        loginPage.logIn(Constants.EMAIL_USER1_ADMIN, Constants.PASSWORD);
        actions.assertElementPresent("navigation.LogOut");
        //TC067 successful logout
        loginPage.logOut();
        Assert.assertTrue(loginPage.isUserLoggedOut());
    }

    //
    @Test
    public void TC068_unsuccessfulLoginWithTypingWrongPassword() {
        loginPage.logInWithWrongPassword();
        actions.assertElementPresent("loginPage.WrongUsernameOrPasswordAlert");
    }

    @Test
    public void TC069_unsuccessfulLoginWithWrongEmail() {
        loginPage.logInWithWrongEmail();
        actions.assertElementPresent("loginPage.WrongUsernameOrPasswordAlert");
    }

    @Test
    public void TC070_unsuccessfulLogInWithoutPassword() {
        loginPage.logInWithEmptyFieldForPassword();
        actions.assertElementPresent("loginPage.WrongUsernameOrPasswordAlert");
    }

    @Test
    public void TC071_unsuccessfulLogInWithoutEmail() {
        loginPage.logInWithEmptyFieldForEmail();
        actions.assertElementPresent("loginPage.WrongUsernameOrPasswordAlert");
    }

    @Test
    public void TC073_successfulRegistrationWithValidCredenitals() throws URISyntaxException {
        actions.registerUser();

        actions.isElementPresentUntilTimeout("REGISTRATION.RegisterLoginButton", 30);
        actions.isElementPresentUntilTimeout("REGISTRATION.SuccessRegistrationMessage", 30);

    }

}
