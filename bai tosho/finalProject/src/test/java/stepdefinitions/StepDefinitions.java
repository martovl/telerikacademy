package stepdefinitions;


import com.telerikacademy.finalproject.utils.UserActions;
import com.telerikacademy.finalproject.utils.Utils;
import org.apache.commons.lang.RandomStringUtils;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.text.MessageFormat;

public class StepDefinitions extends BaseStepDefinitions {
    UserActions actions = new UserActions();

    @Given("Click $element element")
    @When("Click $element element")
    @Then("Click $element element")
    public void clickElement(String element) {
        actions.clickElement(element);
    }

    @Given("Type $value in $name field")
    @When("Type $value in $name field")
    @Then("Type $value in $name field")
    public void typeInField(String value, String field) {
        actions.typeValueInField(value, field);
    }

    @Given("Wait $element element to be present")
    @When("Wait $element element to be present")
    @Then("Wait $element element to be present")
    public void isElementPresentUntilTimeout(String element) {
        actions.isElementPresentUntilTimeout(element, 30);
    }

    @Given("Wait $element element to be clickable")
    @When("Wait $element element to be clickable")
    @Then("Wait $element element to be clickable")
    public boolean waitElementToBeClickable(String element) {
        return actions.waitElementToBeClickable(element, 30);
    }

    @Given("Attach $fileName to $element")
    @When("Attach $fileName to $element")
    @Then("Attach $fileName to $element")
    public void attachFileToElement(String fileName, String element) throws URISyntaxException {
        URL url = getClass().getResource("/UploadFiles/" + fileName);
        File file = Paths.get(url.toURI()).toFile();
        actions.typeValueInField(file.getAbsolutePath(), element);
    }

    @Given("Fill random text with length $length in $field")
    @When("Fill random text with length $length in $field")
    @Then("Fill random text with length $length in $field")
    public void randomText(String length, String field) {
        int lengthText = Integer.parseInt(length);
        String random = RandomStringUtils.randomAlphabetic(lengthText);
        actions.typeValueInField(random, field);
    }

    @Given("Fill random email with length $length in $field")
    @When("Fill random email with length $length in $field")
    @Then("Fill random email with length $length in $field")
    public void randomEmail(String length, String field) {
        int lengthEmail = Integer.parseInt(length);
        String random = RandomStringUtils.randomAlphabetic(lengthEmail);
        actions.typeValueInField(random + "@gmail.com", field);
    }

    @Given("Fill random password in $field and $confirmField")
    @When("Fill random password in $field and $confirmField")
    @Then("Fill random password in $field and $confirmField")
    public void typeRandomPasswordInField(String field, String confirmField) {
        String password = Utils.generateCommonLangPassword();
        actions.typeValueInField(password, field);
        actions.typeValueInField(password, confirmField);
    }

}
