package stepdefinitions;

import com.telerikacademy.finalproject.utils.UserActions;
import org.jbehave.core.annotations.AfterStories;
import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.BeforeStories;
import org.jbehave.core.annotations.BeforeStory;

public class BaseStepDefinitions {
    @BeforeStories
    public void beforeStories(){
        UserActions.loadBrowser("base.url");
    }


    @AfterStories
    public void afterStories(){
        UserActions.quitDriver();
    }

    @BeforeScenario
    public void beforeScenario() {
        UserActions.loadBrowser("base.url");
    }
}



