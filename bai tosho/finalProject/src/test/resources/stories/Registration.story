Meta:
@registration

Narrative:

As a user
I want to fill registration form
So that I can become member of the Social Network


Scenario: SuccessfulRegistrationWithValidCredentials
!--TC073_SuccessfulRegistrationWithValidCredentials

When Click navigation.SignIn element
And Wait REGISTRATION.Link element to be clickable
And Click REGISTRATION.Link element
And Wait REGISTRATION.RegisterButton element to be present
And Fill random email with length 10 in REGISTRATION.EmailInput
And Fill random password in REGISTRATION.PasswordInput and REGISTRATION.PasswordConfirmationInput
And Wait REGISTRATION.PasswordConfirmationInput element to be present
And Fill random text with length 10 in REGISTRATION.FirstNameInput
And Wait REGISTRATION.LastNameInput element to be present
And Fill random text with length 10 in REGISTRATION.LastNameInput
And Click REGISTRATION.Visibility element
And Click REGISTRATION.VisibilityPublic element
And Attach 2.JPG to REGISTRATION.ChooseFileProfilePicture
And Click REGISTRATION.RegisterButton element
Then Wait REGISTRATION.SuccessRegistrationMessage element to be present

Scenario: SuccessfulRegistrationContainingNotRequiredFieldAboutYouWithLength500Characters
!--TC0103_SuccessfulRegistrationContainingNotRequiredFieldAboutYouWithLength500Characters

When Click navigation.SignIn element
And Wait REGISTRATION.Link element to be clickable
And Click REGISTRATION.Link element
And Wait REGISTRATION.RegisterButton element to be present
And Fill random email with length 10 in REGISTRATION.EmailInput
And Fill random password in REGISTRATION.PasswordInput and REGISTRATION.PasswordConfirmationInput
And Wait REGISTRATION.PasswordConfirmationInput element to be present
And Fill random text with length 10 in REGISTRATION.FirstNameInput
And Wait REGISTRATION.LastNameInput element to be present
And Fill random text with length 10 in REGISTRATION.LastNameInput
And Fill random text with length 500 in REGISTRATION.AboutInput
And Click REGISTRATION.Visibility element
And Click REGISTRATION.VisibilityPublic element
And Attach 2.JPG to REGISTRATION.ChooseFileProfilePicture
And Click REGISTRATION.RegisterButton element
Then Wait REGISTRATION.SuccessRegistrationMessage element to be present