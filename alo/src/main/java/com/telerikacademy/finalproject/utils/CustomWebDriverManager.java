package com.telerikacademy.finalproject.utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class CustomWebDriverManager {
    public enum CustomWebDriverManagerEnum {
        INSTANCE;

        private WebDriver driver;
        String chooseBrowser = Utils.getConfigPropertyByKey("chooseBrowser");


        private WebDriver setupBrowser() {

            if (chooseBrowser.equals("chrome")) {
                WebDriverManager.chromedriver().setup();
                WebDriver chromeDriver = new ChromeDriver();
                chromeDriver.manage().window().maximize();
                return chromeDriver;

            } else if (chooseBrowser.contains("firefox")) {
                WebDriverManager.firefoxdriver().setup();
                WebDriver firefoxDriver = new FirefoxDriver();
                firefoxDriver.manage().window().maximize();
                return firefoxDriver;
            }
            throw new IllegalArgumentException();
        }

        public void quitDriver() {
            if (driver != null) {
                driver.quit();
                driver = null; //
            }
        }

        public WebDriver getDriver() {
            if (driver == null) {
                driver = setupBrowser();
            }
            return driver;
        }
    }
}
