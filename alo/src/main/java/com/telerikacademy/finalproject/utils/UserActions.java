package com.telerikacademy.finalproject.utils;

import com.telerikacademy.finalproject.pages.NavigationPage;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class UserActions {
    public WebDriver getDriver() {
        return driver;
    }

    final WebDriver driver;

    public UserActions() {
        this.driver = Utils.getWebDriver();
    }

    public static void loadBrowser(String baseUrlKey) {
        Utils.getWebDriver().get(Utils.getConfigPropertyByKey(baseUrlKey));
    }

    public static void quitDriver() {
        Utils.tearDownWebDriver();
    }

    //############# ELEMENT OPERATIONS #########

    public void clickElement(String key, Object... arguments) {
        String locator = Utils.getUIMappingByKey(key, arguments);

        Utils.LOG.info("Clicking on element " + key);
        WebElement element = driver.findElement(By.xpath(locator));
        element.click();
    }

    public void typeValueInField(String value, String field, Object... fieldArguments) {
        String locator = Utils.getUIMappingByKey(field, fieldArguments);
        WebElement element = driver.findElement(By.xpath(locator));
        element.sendKeys(value);
    }


    public void switchToIFrame(String iframe) {
        WebElement iFrame = driver.findElement(By.xpath(Utils.getUIMappingByKey(iframe)));
        getDriver().switchTo().frame(iFrame);
    }

    public void mouseHoverElement(String key) {
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(key)));
        Actions a = new Actions(driver);
        a.moveToElement(element).perform();
    }

    //############# WAITS #########

    public boolean isElementPresentUntilTimeout(String locator, int timeout, Object... arguments) {
        try {
            String xpath = Utils.getUIMappingByKey(locator, arguments);
            Integer defaultTimeout = Integer.parseInt(Utils.getConfigPropertyByKey("config.defaultTimeoutSeconds"));
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(xpath)));
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    //############# ASSERTS #########

    public void assertElementPresent(String locator) {
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
    }


    //LogIn & LogOut
    public void logIn(String email, String password) {
        Utils.LOG.info("Performing Login method");
        clickElement("navigation.SignIn");
        typeValueInField(email, "loginPage.EmailField");
        typeValueInField(password, "loginPage.PasswordField");
        clickElement("loginPage.LoginButton");
    }

    public void logInWithWrongPassword(){
        Utils.LOG.info("Performing Login method");
        String password = Constants.WRONG_USER_PASSWORD;
        loadBrowser("base.url");
        logIn(Constants.USER_A_EMAIL,password);
    }
    public void logInWithWrongEmail(){
        Utils.LOG.info("Performing Login method");
        String email = Constants.WRONG_USER_EMAIL;
        loadBrowser("base.url");
        logIn(email,Constants.USER_A_PASSWORD);

    }

    public void logOut() {
        Utils.LOG.info("Performing Logout");
        clickElement("navigation.LogOut");
        clickElement("loginPage.HomeButton");
    }
    public boolean isUserLoggedOut() {
        return  isElementPresentUntilTimeout("loginPage.LoginButton", 10);
    }
    public void isUserLoggedIn() {
        boolean isLoggedIn = isElementPresentUntilTimeout("navigation.LogOut", 10);
        Assert.assertTrue(isLoggedIn);
    }

}

