package testCases;

import com.telerikacademy.finalproject.utils.Constants;
import org.jbehave.core.annotations.BeforeScenario;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)


public class LoginSuite extends BaseTest {


    @Test
    public void TC066_successfulLogInAndLogOut() {

        //log in
        actions.logIn(Constants.USER_A_EMAIL, Constants.USER_A_PASSWORD);
        actions.assertElementPresent("navigation.LogOut");

        //log out
        actions.logOut();
        actions.assertElementPresent("navigation.SignIn");


    }

    @Test
    public void TC068_unsuccessfulLoginWithTypingWrongPassword() {
        actions.logInWithWrongPassword();
        actions.assertElementPresent("loginPage.WrongUsernameOrPasswordAlert");
    }

    @Test
    public void TC070_unsuccessfulLoginWithWrongEmail() {
        actions.logInWithWrongEmail();
        actions.assertElementPresent("loginPage.WrongUsernameOrPasswordAlert");
    }

}
