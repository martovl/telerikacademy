package testCases;

import com.telerikacademy.finalproject.pages.LoginPage;
import com.telerikacademy.finalproject.pages.NavigationPage;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class HealthyFoodTestsUSERS extends BaseTest {

    @Test
    public void TC000_navigateToHome_UsingNavigation() {
        NavigationPage navPage = new NavigationPage();
        actions.clickElement(navPage.homeButton);
        navPage.assertPageNavigated();
    }
}

//    @Test
//    public void TC028_memberSuccessfulSendsRequest() {
//
//        LoginPage loginPage = new LoginPage();
//        loginPage.logIn("finalprojectnarnia@gmail.com", "Narniaqa22@");
//        loginPage.isUserLoggedIn();
//
//        actions.isElementPresentUntilTimeout("USERS.SectionUsers",30);
//        actions.clickElement("USERS.SectionUsers");
//        actions.isElementPresentUntilTimeout("USERS.ListUsers", 30);
//        actions.isElementPresentUntilTimeout("USERS.SearchedUser", 10);
//        actions.clickElement("USERS.SearchedUser");
//        actions.isElementPresentUntilTimeout("USERS.ConnectButton", 10);
//        actions.clickElement("USERS.ConnectButton");
//        actions.isElementPresentUntilTimeout("USERS.Sent(Reject)RequestButton", 10);
//
//
//        actions.logOut();
//        Assert.assertTrue(loginPage.isUserLoggedOut());
//    }

//    @Test
//    public void TC029_memberSuccessfulAcceptRequest() {
//
//        LoginPage loginPage = new LoginPage();
//        loginPage.logIn("nevina_82@abv.bg","Narniaqa22@");
//        loginPage.isUserLoggedIn();
//
//        actions.isElementPresentUntilTimeout("USERS.RequestYellowBell",80);
//        actions.mouseHoverElement("USERS.RequestYellowBellDropDown");
//        actions.clickElement("USERS.RequestYellowBellDropDown", 20);
//        actions.isElementPresentUntilTimeout("USERS.AllRequests", 80);
//        actions.clickElement("USERS.RequestSender");
//        actions.isElementPresentUntilTimeout("USERS.ConfirmButton", 80);
//        actions.clickElement("USERS.ConfirmButton");
//        actions.isElementPresentUntilTimeout("USERS.DisconnectButton", 80);
//
//        actions.logOut();
//        Assert.assertTrue(loginPage.isUserLoggedOut());
//    }
//}
