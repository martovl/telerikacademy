

import com.telerikacademy.Queue;
import com.telerikacademy.QueueImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class QueueTests {

    private Queue<Integer> testQueue;

    @BeforeEach
    public void before() {
        testQueue = new QueueImpl<Integer>();
    }

    @Test
    public void Offer_Should_ReturnCorrectElement_When_AddElements() {
        testQueue.offer(1);
        System.out.println(testQueue);

        Assertions.assertEquals(1, 1);
    }

    @Test
    public void Poll_Should_RemoveElement_When_PollElements() {
        testQueue.offer(1);
        testQueue.offer(2);

        Integer removed = testQueue.poll();
        Assertions.assertEquals(1, removed);

    }
    @Test
    public void Poll_Should_Throw_When_SizeIsZero() {

        int size = testQueue.size();

        Assertions.assertEquals(0, size);

    }


    @Test
    public void IsEmpty_Should_ReturnTrue_When_QueueIsEmpty() {
        testQueue.offer(1);
        testQueue.poll();

        boolean isEmpty = testQueue.isEmpty();

        Assertions.assertTrue(isEmpty);
    }

    @Test
    public void IsEmpty_Should_ReturnFalse_When_QueueIsNotEmpty() {
        testQueue.offer(1);


        boolean isEmpty = testQueue.isEmpty();

        Assertions.assertFalse(isEmpty);
    }

    @Test
    public void Peek_Should_ReturnHead_WhenQueueIsNotEmpty() {

        for (int i = 1; i < 5; i++) {
            testQueue.offer(i);
        }

        Integer head = testQueue.peek();

        Assertions.assertEquals(1, head);
    }



    @Test
    public void Size_Should_ReturnCorrectSize_When_QueueIsNotEmpty(){
        for (int i = 0; i < 10 ; i++) {
            testQueue.offer(i);
        }
        Integer result = testQueue.size();

        Assertions.assertEquals(10,result);
    }

}




