package com.telerikacademy.dealership.core.contracts;

public interface Engine {
    
    void start();
    
}
