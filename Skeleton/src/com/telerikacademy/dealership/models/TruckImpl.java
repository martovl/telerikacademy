package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;

import com.telerikacademy.dealership.models.contracts.Truck;

public class TruckImpl extends VehicleBase implements Truck {


    private int weightCapacity;


    public TruckImpl(String make, String model, double price, int weightCapacity ) {
        super(make, model, price, VehicleType.TRUCK);
        setWeightCapacity(weightCapacity);
    }

    public void setWeightCapacity(int weightCapacity) {
        Validator.ValidateIntRange(weightCapacity,
                ModelsConstants.MIN_CAPACITY,
                ModelsConstants.MAX_CAPACITY,
                String.format(ModelsConstants.NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX,
                        "Weight capacity",
                        ModelsConstants.MIN_CAPACITY,
                        ModelsConstants.MAX_CAPACITY)
        );
        this.weightCapacity = weightCapacity;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("  Weight Capacity: %st",getWeightCapacity());
    }
    @Override
    public int getWeightCapacity() {
        return weightCapacity;
    }

    @Override
    public int getWheels() {
        return getVehicleType().getWheelsFromType();
    }

    @Override
    public VehicleType getType() {
        return VehicleType.TRUCK;
    }

}
