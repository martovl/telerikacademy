package com.telerikacademy.dealership.models.contracts;

public interface Comment {
    
    String getContent();
    
    String getAuthor();
    
}
