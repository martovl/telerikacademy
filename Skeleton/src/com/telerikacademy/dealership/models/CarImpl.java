package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Car;


public class CarImpl extends VehicleBase implements Car {
    private int seats;

    public CarImpl(String make, String model, double price,int seats ) {
        super(make, model, price, VehicleType.CAR);
        setSeats(seats);
    }

    public void setSeats(int seats) {
        Validator.ValidateIntRange(seats,
                ModelsConstants.MIN_SEATS,
                ModelsConstants.MAX_SEATS,
                String.format(ModelsConstants.NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX,
                        "Seats",
                        ModelsConstants.MIN_SEATS,
                        ModelsConstants.MAX_SEATS)
        );

        this.seats = seats;
    }

    @Override
    protected String printAdditionalInfo() {
       return String.format("  Seats: %s",getSeats());
    }

    @Override
    public int getSeats() {
        return seats;
    }

    @Override
    public int getWheels() {

        return getVehicleType().getWheelsFromType();
    }

    @Override
    public VehicleType getType() {
        return VehicleType.CAR;
    }

    
}
