package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Utils;
import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Vehicle;

import java.util.ArrayList;
import java.util.List;

public abstract class VehicleBase implements Vehicle {
    
    private final static String MAKE_FIELD = "Make";
    private final static String MODEL_FIELD = "Model";
    private final static String PRICE_FIELD = "Price";
    private final static String WHEELS_FIELD = "Wheels";
    private final static String COMMENTS_HEADER = "    --COMMENTS--";
    private final static String NO_COMMENTS_HEADER = "    --NO COMMENTS--";

    //add fields
    private String make;
    private String model;
    private double price;
    private VehicleType vehicleType;
    private List<Comment> comments;
    protected int wheels;
    
    // finish the constructor and validate input; look in package com.telerikacademy.dealership.models.common.enums; what methods are there in VehicleType?
    VehicleBase(String make, String model, double price, VehicleType vehicleType) {
        setMake(make);
        setModel(model);
        setPrice(price);
        setVehicleType(vehicleType);
        comments = new ArrayList<>();

    }

    @Override
    public int getWheels() {
        return wheels;
    }

    @Override
    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        Validator.ValidateNull(make,String.format(ModelsConstants.FIELD_CANNOT_BE_NULL,MAKE_FIELD));

        Validator.ValidateIntRange(make.length(),
                ModelsConstants.MIN_MAKE_LENGTH,
                ModelsConstants.MAX_MAKE_LENGTH,
                String.format(ModelsConstants.STRING_MUST_BE_BETWEEN_MIN_AND_MAX,
                        MAKE_FIELD,
                        ModelsConstants.MIN_MAKE_LENGTH,
                        ModelsConstants.MAX_MAKE_LENGTH)
            );

        this.make = make;
    }

    @Override
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        Validator.ValidateNull(model, String.format(ModelsConstants.FIELD_CANNOT_BE_NULL, MODEL_FIELD));

        Validator.ValidateIntRange(model.length(),
                ModelsConstants.MIN_MODEL_LENGTH,
                ModelsConstants.MAX_MODEL_LENGTH,
                String.format(ModelsConstants.STRING_MUST_BE_BETWEEN_MIN_AND_MAX,
                        MODEL_FIELD,
                        ModelsConstants.MIN_MODEL_LENGTH,
                        ModelsConstants.MAX_MODEL_LENGTH)
                );

        this.model = model;
    }

    @Override
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        Validator.ValidateDecimalRange(price,
                ModelsConstants.MIN_PRICE,
                ModelsConstants.MAX_PRICE,
                String.format(ModelsConstants.NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX,
                    PRICE_FIELD,
                    ModelsConstants.MIN_PRICE,
                    ModelsConstants.MAX_PRICE)
        );
        this.price = price;
    }

    public VehicleType getVehicleType() { return vehicleType; }

    public void setVehicleType(VehicleType vehicleType) { this.vehicleType = vehicleType; }

    @Override
    public List<Comment> getComments() {
        return comments;
    }

    @Override
    public void removeComment(Comment comment) {
        getComments().remove(comment);

    }

    @Override
    public void addComment(Comment comment) {
        getComments().add(comment);

    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        
        builder.append(String.format("%s:", this.getClass().getSimpleName().replace("Impl", ""))).append(System.lineSeparator());
        //finish implementation of toString() - what should the next 3 lines of code append to the builder?
        builder.append(String.format("  Make: %s" , getMake())).append(System.lineSeparator());
        builder.append(String.format("  Model: %s", getModel())).append(System.lineSeparator());
        builder.append(String.format("  Wheels: %s", getWheels())).append(System.lineSeparator());
        
        builder.append(String.format("  %s: $%s", PRICE_FIELD, Utils.removeTrailingZerosFromDouble(price))).append(System.lineSeparator());
        
        if (!printAdditionalInfo().isEmpty()) {
            builder.append(printAdditionalInfo()).append(System.lineSeparator());
        }
        builder.append(printComments());
        return builder.toString();
    }
    
    //we make it protected because we want to give access only to child classes that extends Vehicle base.
    protected abstract String printAdditionalInfo();
    
    private String printComments() {
        StringBuilder builder = new StringBuilder();
        
        if (comments.size() <= 0) {
            builder.append(String.format("%s", NO_COMMENTS_HEADER));
        } else {
            builder.append(String.format("%s", COMMENTS_HEADER)).append(System.lineSeparator());
            
            for (Comment comment : comments) {
                builder.append(comment.toString());
            }
            
            builder.append(String.format("%s", COMMENTS_HEADER));
        }
        
        return builder.toString();
    }

    
}
