package tests.models;

import com.telerikacademy.dealership.models.CarImpl;
import com.telerikacademy.dealership.models.contracts.Car;
import com.telerikacademy.dealership.models.contracts.Vehicle;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class CarImpl_Tests {
    
    
    @Test
    public void CarImpl_ShouldImplementCarInterface() {
        CarImpl car = new CarImpl("make", "model", 100, 4);
        // Assert
        Assertions.assertTrue(car instanceof Car);
    }
    
    @Test
    public void CarImpl_ShouldImplementVehicleInterface() {
        CarImpl car = new CarImpl("make", "model", 100, 4);
        // Assert
        Assertions.assertTrue(car instanceof Vehicle);
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenMakeIsNull() {
        // Act
        Assertions.assertThrows(NullPointerException.class,
                () -> new CarImpl(null, "model", 100, 4));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenMakeLengthIsBelow2() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CarImpl("m", "model", 100, 4));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenMakeLengthIsAbove15() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CarImpl("makeLongCar12345", "model", 100, 4));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenModelIsNull() {
        // Act
        Assertions.assertThrows(NullPointerException.class,
                () -> new CarImpl("make", null, 100, 4));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenModelLengthIsBelow_1() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CarImpl("make", "", 100, 4));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenModelLengthIsAbove15() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CarImpl("make", "longLongModel123", 100, 4));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenPriceIsNegative() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CarImpl("make", "model", -100, 4));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenPriceIsAbove1000000() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CarImpl("make", "model", 1000001, 4));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenSeatsIsNegative() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CarImpl("make", "model", 1000, -4));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenSeatsIsAbove10() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CarImpl("make", "model", 1000, 11));
    }
    
}
