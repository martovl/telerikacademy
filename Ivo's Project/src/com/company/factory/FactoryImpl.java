package com.company.factory;


import com.company.core.Factories;
import com.company.helpers.DoctorateTypes;
import com.company.members.Doctorate;
import com.company.members.Students;
import com.company.members.Teachers;
import com.company.members.UniversityImpl;

public class FactoryImpl implements Factories {


    @Override
    public UniversityImpl createUniversity(String name, String city) {
        return new UniversityImpl(name, city);
    }

    @Override
    public Students createStudent(String firstName, String lastName, int age, int phoneNumber, int grade) {
        return new Students(firstName, lastName, age, phoneNumber,grade);
    }

    @Override
    public Doctorate createDoctorate(String firstName, String lastName, int age, int phoneNumber,
                                     String doctorate, int salary, DoctorateTypes types) {
        return new Doctorate(firstName,lastName,age,phoneNumber,doctorate,salary,types);
    }

    @Override
    public Teachers createTeacher(String firstName, String lastName, int age, int phoneNumber,
                                  String subject, int yearsExperience, int salary) {
        return new Teachers(firstName, lastName, age, phoneNumber, subject, yearsExperience, salary);
    }
}
