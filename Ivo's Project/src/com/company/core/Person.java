package com.company.core;

import com.company.core.Print;

public interface Person extends Print {

    String getFirstName();

    String getLastName();

    int getAge();

    long getPhoneNumber();
}
