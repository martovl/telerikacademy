package com.company.core;

public interface University {

    String getName();

    String getCity();

}
