package com.company.core;


import com.company.helpers.DoctorateTypes;
import com.company.members.Doctorate;
import com.company.members.Students;
import com.company.members.Teachers;
import com.company.members.UniversityImpl;

public interface Factories {

    UniversityImpl createUniversity(String name, String city);

    Students createStudent(String firstName, String lastName, int age, int phoneNumber, int grade );

    Doctorate createDoctorate(String firstName, String lastName, int age, int phoneNumber,
                              String doctorate, int salary, DoctorateTypes types);



    Teachers createTeacher ( String firstName, String lastName, int age, int phoneNumber,
                             String subject, int yearsExperience, int salary);















}
