package com.company.helpers;

public class Validator {
    public static void ValidateIntRange(int value, int min, int max, String message) {
        if (value < min || value > max) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void ValidateNull(Object value, String message) {
        if (value == null) {
            throw new NullPointerException(message);
        }
    }
}
