package com.company.helpers;

public enum DoctorateTypes {
    Mathematics,
    Philosophy,
    Medicine;

    @Override
    public String toString() {
        switch (this) {
            case Mathematics:
                return "Mathematics";
            case Philosophy:
                return "Philosophy";
            case Medicine:
                return "Medicine";
            default:
                throw new IllegalArgumentException();
        }

    }
}
