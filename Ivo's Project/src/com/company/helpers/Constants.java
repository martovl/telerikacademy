package com.company.helpers;

public class Constants {
    public final static int MIN_NAME_LENGTH = 2;
    public final static int MAX_NAME_LENGTH = 20;

    public final static int MIN_AGE = 18;
    public final static int MAX_AGE = 60;

    public final static int MIN_GRADE = 2;
    public final static int MAX_GRADE = 6;

    public final static int MIN_PHONE_LENGTH = 9;
    public final static int MAX_PHONE_LENGTH = 11;

    public final static int MIN_YEARS_EXPERIENCE_TEACHER = 1;
    public final static int MAX_YEARS_EXPERIENCE_TEACHER = 15;

    public final static int MIN_SALARY_TEACHER = 500;
    public final static int MAX_SALARY_TEACHER = 1500;

    public final static int MIN_SALARY_DOCTORATE = 2000;
    public final static int MAX_SALARY_DOCTORATE = 5000;

    public final static String NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX = "%s must be between %s and %s!";

    public final static String FIELD_CANNOT_BE_NULL = "%s cannot be null!";
}
