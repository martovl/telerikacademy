package com.company;

import com.company.factory.FactoryImpl;
import com.company.helpers.DoctorateTypes;
import com.company.members.Teachers;

public class Main {

    public static void main(String[] args) {

        // Create object with Teachers constructor

//        Teachers teachers = new Teachers("Ivo","Lazarov",23,89987648,"Python",2,1000);
//
//        System.out.printf("First Name: %s%nLast name: %s%nAge: %s%nPhone number:" +
//                        " %s%nSubject: %s%nExperience: %s%nSalary:%s%n",
//                teachers.getFirstName(),teachers.getLastName(),teachers.getAge(),teachers.getPhoneNumber(),
//                teachers.getSubject(),teachers.getYearsExperience(),teachers.getSalary());
//    }
        // Create with Factory

        FactoryImpl factory = new FactoryImpl();

        factory.createTeacher("Ivo","Lazarov",23,89987648,"Python",2,1000);
        factory.createDoctorate("Martin","Vlachkov",22,899776479,"Java",4999, DoctorateTypes.Mathematics);



    }
}
