package com.company.members;

import com.company.helpers.Constants;
import com.company.helpers.Validator;

public class Teachers extends PersonBase{

    private String subject;
    private int yearsExperience;
    private int salary;


    public Teachers(String firstName, String lastName, int age, int phoneNumber,
                    String subject, int yearsExperience, int salary) {
        super(firstName, lastName, age, phoneNumber);
        setSubject(subject);
        setYearsExperience(yearsExperience);
        setSalary(salary);
    }


    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        Validator.ValidateNull(subject, String.format(Constants.FIELD_CANNOT_BE_NULL, subject));
        this.subject = subject;
    }

    public int getYearsExperience() {
        return yearsExperience;
    }

    public void setYearsExperience(int yearsExperience) {
        Validator.ValidateIntRange(yearsExperience,
                Constants.MIN_YEARS_EXPERIENCE_TEACHER,
                Constants.MAX_YEARS_EXPERIENCE_TEACHER,
                String.format(Constants.NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX,
                        yearsExperience,
                        Constants.MIN_YEARS_EXPERIENCE_TEACHER,
                        Constants.MAX_YEARS_EXPERIENCE_TEACHER)
        );
        this.yearsExperience = yearsExperience;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        Validator.ValidateIntRange(salary,
                Constants.MIN_SALARY_TEACHER,
                Constants.MAX_SALARY_TEACHER,
                String.format(Constants.NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX,
                        salary,
                        Constants.MIN_SALARY_TEACHER,
                        Constants.MAX_SALARY_TEACHER)
        );
        this.salary = salary;
    }

}
