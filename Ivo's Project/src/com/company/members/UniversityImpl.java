package com.company.members;

import com.company.core.University;
import com.company.helpers.Constants;
import com.company.helpers.Validator;

public class UniversityImpl implements University {
    private String name;
    private String city;


    public UniversityImpl(String name, String city) {
        setName(name);
        setCity(city);
    }
    public void setName(String name) {
        Validator.ValidateNull(name, String.format(Constants.FIELD_CANNOT_BE_NULL, name));
        Validator.ValidateIntRange(name.length(),
                Constants.MIN_NAME_LENGTH,
                Constants.MAX_NAME_LENGTH,
                String.format(Constants.NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX,
                        name,
                        Constants.MIN_NAME_LENGTH,
                        Constants.MAX_NAME_LENGTH)
        );
        this.name = name;
    }

    public void setCity(String city) {
        Validator.ValidateNull(city, String.format(Constants.FIELD_CANNOT_BE_NULL, city));
        Validator.ValidateIntRange(name.length(),
                Constants.MIN_NAME_LENGTH,
                Constants.MAX_NAME_LENGTH,
                String.format(Constants.NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX,
                        city,
                        Constants.MIN_NAME_LENGTH,
                        Constants.MAX_NAME_LENGTH)
        );
        this.city = city;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getCity() {
        return city;
    }
}
