package com.company.members;

import com.company.helpers.Constants;
import com.company.helpers.Validator;

public class Students extends PersonBase {
    private int grade;

    public Students(String firstName, String lastName, int age, int phoneNumber,int grade) {
        super(firstName, lastName, age, phoneNumber);
        setGrade(grade);
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        Validator.ValidateIntRange(grade,
                Constants.MIN_GRADE,
                Constants.MAX_GRADE,
                String.format(Constants.NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX,
                        grade,
                        Constants.MIN_GRADE,
                        Constants.MAX_GRADE)
        );
        this.grade = grade;
    }
}
