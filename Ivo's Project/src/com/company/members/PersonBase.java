package com.company.members;

import com.company.core.Person;
import com.company.helpers.Constants;
import com.company.helpers.Validator;

public class PersonBase implements Person {
    private String firstName;
    private String lastName;
    private int age;
    private int phoneNumber;

    public PersonBase(String firstName, String lastName, int age, int phoneNumber) {
        setFirstName(firstName);
        setLastName(lastName);
        setAge(age);
        setPhoneNumber(phoneNumber);
    }

    public void setFirstName(String firstName) {
        Validator.ValidateNull(firstName, String.format(Constants.FIELD_CANNOT_BE_NULL, firstName));
        Validator.ValidateIntRange(firstName.length(),
                Constants.MIN_NAME_LENGTH,
                Constants.MAX_NAME_LENGTH,
                String.format(Constants.NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX,
                        firstName,
                        Constants.MIN_NAME_LENGTH,
                        Constants.MAX_NAME_LENGTH)
        );
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        Validator.ValidateNull(lastName, String.format(Constants.FIELD_CANNOT_BE_NULL, lastName));
        Validator.ValidateIntRange(lastName.length(),
                Constants.MIN_NAME_LENGTH,
                Constants.MAX_NAME_LENGTH,
                String.format(Constants.NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX,
                        lastName,
                        Constants.MIN_NAME_LENGTH,
                        Constants.MAX_NAME_LENGTH)
        );
        this.lastName = lastName;
    }

    public void setAge(int age) {
        Validator.ValidateIntRange(age,
                Constants.MIN_AGE,
                Constants.MAX_AGE,
                String.format(Constants.NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX,
                        age,
                        Constants.MIN_AGE,
                        Constants.MAX_AGE)
        );
        this.age = age;
    }
    // Must validate symbols not the whole number - between 9-11 symbols!
    public void setPhoneNumber(int phoneNumber) {
//        Validator.ValidateIntRange(phoneNumber,
//                Constants.MIN_PHONE_LENGTH,
//                Constants.MAX_PHONE_LENGTH,
//                String.format(Constants.NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX,
//                        phoneNumber,
//                        Constants.MIN_PHONE_LENGTH,
//                        Constants.MAX_PHONE_LENGTH)
//        );
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public int getAge() {
        return age;
    }

    @Override
    public long getPhoneNumber() {
        return phoneNumber;
    }

    @Override
    public String printBase() {return null;}
}
