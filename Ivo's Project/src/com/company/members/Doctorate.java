package com.company.members;

import com.company.helpers.Constants;
import com.company.helpers.DoctorateTypes;
import com.company.helpers.Validator;

public class Doctorate extends PersonBase {
    private String doctorate;
    private int salary;
    private DoctorateTypes type;

    public Doctorate(String firstName, String lastName, int age, int phoneNumber,
                     String doctorate, int salary, DoctorateTypes type) {
        super(firstName, lastName, age, phoneNumber);
        setSalary(salary);
        setDoctorate(doctorate);
        setType(type);
    }

    public DoctorateTypes getType() {
        return type;
    }

    public void setType(DoctorateTypes type) {
        this.type = type;
    }

    public String getDoctorate() {
        return doctorate;
    }

    public void setDoctorate(String doctorate) {
        Validator.ValidateNull(doctorate, String.format(Constants.FIELD_CANNOT_BE_NULL,doctorate));
        this.doctorate = doctorate;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        Validator.ValidateIntRange(salary,
                Constants.MIN_SALARY_DOCTORATE,
                Constants.MAX_SALARY_DOCTORATE,
                String.format(Constants.NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX,
                        salary,
                        Constants.MIN_SALARY_DOCTORATE,
                        Constants.MAX_SALARY_DOCTORATE));
        this.salary = salary;
    }
}
